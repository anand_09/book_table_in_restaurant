package com.sapient.rest.booktable;

import java.io.IOException;
import java.net.URI;

import javax.json.JsonObject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import com.sapient.exception.AddressNullPointerException;
import com.booktable.others.RBundle;

/**
 * Servlet implementation class table
 */
public class table extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public table() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String address = request.getParameter("address");
			if(address == null) {
				throw new AddressNullPointerException(RBundle.getValues("Error1"));
			}
			ClientConfig locConfig = new ClientConfig();
			Client c = ClientBuilder.newClient(locConfig);
			WebTarget webTarget = c.target(getGeoCode(address));
			
			JsonObject geoJson = webTarget.request().accept(MediaType.APPLICATION_JSON).get(JsonObject.class);  
			System.out.println(geoJson);
			
			javax.json.JsonArray location = geoJson.getJsonArray("results");
			JsonObject geometry = (JsonObject) ((JsonObject) location.get(0)).get("geometry");
			JsonObject locationJson = geometry.getJsonObject("location");
			String latitude = locationJson.get("lat").toString();
			String longitude = locationJson.get("lng").toString();
			
			ClientConfig config = new ClientConfig();
			Client client = ClientBuilder.newClient(config);
			WebTarget target = client.target(getBaseUri(latitude,longitude));
			JsonObject resJson=target.request().accept(MediaType.APPLICATION_JSON).get(JsonObject.class);
			request.setAttribute("restaurants",resJson);
			System.out.println(resJson);
			RequestDispatcher rd=request.getRequestDispatcher("Restaurants.jsp");
			request.setAttribute("Restaurants", resJson);
			rd.forward(request, response);
		}
		catch(AddressNullPointerException r) {
			RequestDispatcher rs = request.getRequestDispatcher("rest.jsp");
			request.setAttribute("Restaurants", r.getMessage());
			rs.forward(request, response);
		}
		
		 
	}
	
	private static URI getBaseUri(String latitude,String longitude){
		 return UriBuilder.fromUri("https://developers.zomato.com/api/v2.1/geocode?apikey=9bdd710efc3332ced1c1b0024272b234&lat="+latitude+"&lon="+longitude).build();
	}
	
	private static URI getGeoCode(String address) {
		String finalString = "";
		for(int i=0;i<address.length();i++) {
			 if(address.charAt(i)==' ' || address.charAt(i)==',')
				 finalString+="+";
			 else 
				 finalString+=address.charAt(i);
		 }
		System.out.println(finalString);
		return UriBuilder.fromUri("https://maps.googleapis.com/maps/api/geocode/json?address="+finalString+"&key=AIzaSyBpv7_tKVO5svTnTDUdfWz-fM3qpash6Gw").build();
	}

}
