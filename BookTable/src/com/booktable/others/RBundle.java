package com.booktable.others;

import java.util.ResourceBundle;

public class RBundle {
	public static String getValues(String key) {
		ResourceBundle rs = ResourceBundle.getBundle("restaurant");
		return rs.getString(key);
	}
}
